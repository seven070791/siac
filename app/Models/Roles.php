<?php 
namespace App\Models;

class Roles extends BaseModel{

	//Metodo para obtener los roles registrados

	public function getRoles($idrol) {
		$builder = $this->dbconn('sgc_roles r'); // assuming dbconn returns a query builder instance
		$whereConditions = [];
		if ($idrol != 5) {
			$whereConditions['r.idrol !='] = 5;
		}
		$whereConditions['borrado'] = false;
		$builder->where($whereConditions);
		$query = $builder->get();
		if (!$query) {
			// handle query error or exception
			throw new Exception('Error retrieving roles');
		}
		return $query;
	}
}