<!-- Content Wrapper. Contains page content -->

<script type="text/javascript" src="<?php echo base_url(); ?>/js_paginas/Chart.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/js_paginas/jspdf.debug.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/chart.js@latest/dist/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script> -->
<link rel="stylesheet" href="<?php echo base_url(); ?>/css_paginas/estadisticas.css">


<style>
 table {
  border-collapse: collapse;
  width: 100%; /* Ajusta el ancho según sea necesario */
}
th, td {
  border: 1px solid #ddd;
  padding: 6px;
  text-align: left;
}
th {
  background-color: #f2f2f2;
}
tr:nth-child(even) {
  background-color: #f2f2f2;
}


</style>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Estadísticas Globales</h1>
        </div>
        <div class="col-sm-6">
          &nbsp;&nbsp; <label for="min">Desde</label>&nbsp;
          <input type="date" class="bodersueve"    style="width:150px;"  value="<?php echo date('YY-MM-DD'); ?>" name="desde" id="desde">&nbsp;&nbsp;
          <label for="hasta">Hasta</label>&nbsp;&nbsp;
          <input type="date" class="bodersueve" style="width:150px;" value="<?php echo date('YY-MM-DD'); ?>" name="hasta" id="hasta">&nbsp;
          &nbsp;&nbsp;<button type="button" class="btn btn-sm btn-primary consultar">Consultar</button>
          &nbsp;&nbsp;<button type="button" class="btn btn-sm btn-secondary limpiar">Limpiar</button>
        </div>
      </div>

      <div class="row mb-2">
        <div class="col-sm-6">
        
        </div>
        &nbsp;&nbsp; &nbsp;&nbsp;<label for="estado-caso">Estado</label>
        <div class="col-lg-3 col-sm-3 col-md-3">
          <select id="estado-caso" name="estado-caso" class="form-control">
              <option value="0" disabled>Seleccione Estado</option>
          </select>
        </div>
      </div>


   <!-- /.container-fluid -->
    <div class="fechas"style="display: block;" >
 <label for="hasta">Desde: </label>&nbsp;&nbsp;
    <input type="text" class="fecha" name="" disabled="disabled" style="width:100px;" id="fecha_desde" value="php echo $fecha_desde; ?>">
    <label for="hasta">Hasta: </label>&nbsp;&nbsp;
    <input type="text" class="fecha" name="" disabled="disabled" style="width:100px;" id="fecha_hasta" value="php echo $fecha_hasta; ?>">

    </div>
   
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="card">
      <form id="anual-report" name="anual-report" method="POST" class="form-horizontal">
        <!-- /.card -->
    
        <div id="reportPage">
          <div class="row">

            <div class="col-md-5">
              <div class="card">

                <div class="card-header">
                  <h3 class="card-title">Via de Atención</h3>

                  <div class="card-tools">

                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                      <i class="fas fa-times"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <canvas id="grafica"></canvas>
                </div>
              </div>

            </div>

            <div class="col-md-2">
            </div>
            <div class="col-md-5">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Casos por Propiedad Intelectual</h3>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                      <i class="fas fa-times"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <canvas id="grafica_prointel"></canvas>
                </div>
              </div>
            </div>
          </div>
        </div>
  </section>

  </section>



  <div class="card">
    <form id="anual-report" name="anual-report" method="POST" class="form-horizontal">
      <!-- /.card -->
      <div class="row">
      <div class="col-md-4 card">
        <div class="card">
          <div class="card-header">
            <h4 class="text-primary"><i class="fas fa-angle-double-right"></i>  Estatus de Audiencia</h4>
          </div>
          <div class="card-body">
            <div class="text-muted">
              <table class="diseño">
                <thead><tr><th>Estatus</th><th>Cantidad</th></tr></thead>
                <tbody>
                  <?php $total = 0; ?>
                  <?php foreach ($estatus['requerimientosbyEstados'] as $estado) { ?>
                  <tr>
                    <td><?php echo $estado['estado']; ?></td>
                    <td><?php echo $estado['total']; ?></td>
                  </tr>
                  <?php $total += $estado['total']; ?>
                  <?php } ?>
                  <tr>
                    <td><strong>Total</strong></td>
                    <td><strong><?php echo $total; ?></strong></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>





<div class="col-md-4 card">
  <div class="card">
    <div class="card-header">
      <h4 class="text-primary"><i class="fas fa-angle-double-right"></i> Vía de Atención</h4>
    </div>
    <div class="card-body">
      <div class="text-muted">
        <table class="diseño">
          <thead><tr><th>Vía de Atención</th><th>Cantidad</th></tr></thead>
          <tbody>
            <?php
            $formatos_cita = array();
            foreach ($audiencias['requerimientos'] as $requerimiento) {
              $formato_cita_nombre = $requerimiento['formato_cita'];
              if (!isset($formatos_cita[$formato_cita_nombre])) {
                $formatos_cita[$formato_cita_nombre] = 0;
              }
              $formatos_cita[$formato_cita_nombre]++;
            }
            foreach ($formatos_cita as $formato => $cantidad) {
              echo '<tr><td>' . $formato . '</td><td>' . $cantidad . '</td></tr>';
            }
            ?>
            <tr><td>Total Citas</td><td><?php echo count($audiencias['requerimientos']); ?></td></tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="col-md-4 card">
  <div class="card">
    <div class="card-header">
      <h4 class="text-primary"><i class="fas fa-angle-double-right"></i> Propiedad Intelectual</h4>
    </div>
    <div class="card-body">
      <div class="text-muted">
        <table class="diseño">
          <thead><tr><th>Tipo de Propiedad Intelectual</th><th>Cantidad</th></tr></thead>
          <tbody>
            <?php
            $areas = array();
            foreach ($audiencias['requerimientos'] as $requerimiento) {
              $area = $requerimiento['area'];
              if (!isset($areas[$area])) {
                $areas[$area] = 0;
              }
              $areas[$area]++;
            }
            foreach ($areas as $area => $cantidad) {
              echo '<tr><td>' . $area . '</td><td>' . $cantidad . '</td></tr>';
            }
            ?>
            <tr><td>Total Casos</td><td><?php echo count($audiencias['requerimientos']); ?></td></tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
      </div>
    </form>
  </div>


  </section>

  <?php

$Presencial = 0;
$Virtual = 0;
$Total = 0;

foreach ($audiencias['requerimientos'] as $audiencia) {
    $Total++;
    if ($audiencia['formato_cita'] == "PRESENCIAL") {
        $Presencial++;
    } elseif ($audiencia['formato_cita'] == "VIRTUAL") {
        $Virtual++;
    }
}
?>

<script>
  // Obtener una referencia al elemento canvas del DOM
  const $grafica = document.querySelector("#grafica");
  // Las etiquetas son las que van en el eje X. 
  const etiquetas = ["CASOS ATENDIDOS"]
  // Podemos tener varios conjuntos de datos. Comencemos con uno
  const datosVentas2020 = {
    label: "CASOS ATENDIDOS",
    data: [<?php echo $Total; ?>], // La data es un arreglo que debe tener la misma cantidad de valores que la cantidad de etiquetas // La data es un arreglo que debe tener la misma cantidad de valores que la cantidad de etiquetas
    backgroundColor: [
      'rgba(54, 162, 235, 0.2)', // Color de fondo para CASOS ATENDIDOS
      'rgba(255, 99, 132, 0.5)', // Color de fondo para VIRTUAL
      'rgba(255, 206, 86, 0.5)' // Color de fondo para PRESENCIAL
    ],
    borderColor: [
      'rgba(54, 162, 235, 1)', // Color del borde para CASOS ATENDIDOS
      'rgba(255, 99, 132, 1)', // Color del borde para VIRTUAL
      'rgba(255, 206, 86, 1)' // Color del borde para PRESENCIAL
    ],
    borderWidth: 1, // Ancho del borde
  };

  new Chart($grafica, {
    type: 'bar', // Tipo de gráfica
    data: {
      labels: etiquetas,
      datasets: [
        datosVentas2020,
        {
            label: 'Presencial',
            data: [<?php echo $Presencial; ?>],
            backgroundColor: 'rgba(255, 206, 86, 0.5)'
            // Color de fondo de las barras para el segundo conjunto de datos
          },

        {
            label: 'Virtual',
            data: [<?php echo $Virtual; ?> ],
            backgroundColor: 'rgba(255, 99, 132, 0.5)' // Color de fondo de las barras para el segundo conjunto de datos
          }
          
        // Aquí más datos...
      ]
    },
    options: {
      responsive: true,
      title: {
        display: true,

      },
      tooltips: {
        mode: "index",
        intersect: false
      },

      scales: {
        xAxes: [{
          ticks: {
            beginAtZero: true,
            stepSize: 2
          }
        }]
      }
    }
  });
</script>




<?php



$Marcas = 0;
$Patentes = 0;
$Total_prointel = 0;

foreach ($audiencias['requerimientos'] as $audiencia) {
    $Total_prointel++;
    if ($audiencia['area'] == "MARCAS") {
        $Marcas++;
    } elseif ($audiencia['area'] == "PATENTES") {
        $Patentes++;
    }
}


?>



<script>
  // Obtener una referencia al elemento canvas del DOM
  const $grafica_prointel = document.querySelector("#grafica_prointel");
  // Las etiquetas son las que van en el eje X. 
  const etiquetas_prointel = ["CASOS ATENDIDOS"]
  // Podemos tener varios conjuntos de datos. Comencemos con uno
  const datosVentas2020_prointel = {
    label: "CASOS ATENDIDOS",
    data: [<?php echo $Total_prointel; ?>], // La data es un arreglo que debe tener la misma cantidad de valores que la cantidad de etiquetas // La data es un arreglo que debe tener la misma cantidad de valores que la cantidad de etiquetas
    backgroundColor: [
      'rgba(54, 162, 235, 0.2)', // Color de fondo para CASOS ATENDIDOS
      'rgba(255, 99, 132, 0.5)', // Color de fondo para VIRTUAL
      'rgba(255, 206, 86, 0.5)' // Color de fondo para PRESENCIAL
    ],
    borderColor: [
      'rgba(54, 162, 235, 1)', // Color del borde para CASOS ATENDIDOS
      'rgba(255, 99, 132, 1)', // Color del borde para VIRTUAL
      'rgba(255, 206, 86, 1)' // Color del borde para PRESENCIAL
    ],
    borderWidth: 1, // Ancho del borde
  };

  new Chart($grafica_prointel, {
    type: 'bar', // Tipo de gráfica
    data: {
      labels: etiquetas_prointel,
      datasets: [
        datosVentas2020_prointel,
        {
            label: 'Marcas',
            data: [<?php echo $Marcas; ?>],
            backgroundColor: 'rgba(255, 206, 86, 0.5)'
            // Color de fondo de las barras para el segundo conjunto de datos
          },
        {
            label: 'Patentes',
            data: [<?php echo $Patentes; ?> ],
            backgroundColor: 'rgba(255, 206, 86, 0.5))' // Color de fondo de las barras para el segundo conjunto de datos
          }
          
        // Aquí más datos...
      ]
    },
    options: {
      responsive: true,
      title: {
        display: true,

      },
      tooltips: {
        mode: "index",
        intersect: false
      },

      scales: {
        xAxes: [{
          ticks: {
            beginAtZero: true,
            stepSize: 2
          }
        }]
      }
    }
  });
</script>





