<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema Integrado de Atención al Ciudadano (SIAC)</title>
  <link rel="icon" type="image/png" href="<?= base_url() ?>/img/LogoSIAC_sapi2.png" style="width: 50px; height: 50px;">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/plugins/daterangepicker/daterangepicker.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/css/adminlte.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/css/adminlte.min.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- DataTables -->
  
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css_paginas/botones_datatable.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/datatable_responsive/css/fixedHeader.bootstrap.min.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>/datatable_responsive/css/responsive.bootstrap.min.css">
</head>